This is my dotfiles repo, with
    *bashrc - pretty much the standard Elementary OS bashrc
        *bash_login - runs on login, calls bashrc
        *bash_aliases - personal aliases
    *tmux.conf
    *vimrc
